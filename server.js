const express = require("express")
const app = express()

app.set('view engine', 'ejs')

app.use(express.json())

app.get("/", (req, res) => {
    res.render('index')
})

const starWarsRouter = require("./routes/starwars")
app.use("/starwars", starWarsRouter)

app.listen(3000)