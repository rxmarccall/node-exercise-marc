const express = require('express')
const axios = require('axios')
const router = express.Router()

const baseUrl = 'https://swapi.dev/api/';

router.get('/people', async (req, res, next) => {
    res.render('people')
    processPeopleRequest(req);
    next();
})

async function processPeopleRequest(req)
{
    console.log("Fetching Star Wars People From SWAPI \n");

    let people = [];
    let currentRequestUrl = baseUrl + 'people/';

    let peopleRequest;

    // Process requests until we've received all people data
    do {
        peopleRequest = await axios.get(currentRequestUrl);
        currentRequestUrl = peopleRequest.data.next;

        // Capture people from current request
        for (let i = 0; i < peopleRequest.data.results.length; ++i)
        {
            people[people.length] = peopleRequest.data.results[i];
        }
    }
    while (peopleRequest.data.next != null)

    // Check if request wanted people to be sorted
    if (req.query.sortBy !== undefined)
    {
        console.log("Sort People By: " + req.query.sortBy);
        
        switch (req.query.sortBy) {
            case 'name':
                people = people.sort((a, b) => {
                    return a.name.localeCompare(b.name);
                })
            break;
            case 'height':
            {
                // Strip out "unknown" values for sorting
                people.forEach(x => {
                    if (x.height === "unknown")
                        x.height = "0";
                });  
            
                // Sort numbers ascending
                people.sort((a, b) => {
                    return a.height - b.height;
                });

                // Restore "unknown" values after sorting
                people.forEach(x => {
                    if (x.height === "0")
                        x.height = "unknown";
                });  
                
                break;
            }
            case 'mass':
            {   
                // Strip out "unknown" values and commas for sorting
                people.forEach(x => {
                    if (x.mass === "unknown")
                        x.mass = "0";

                    x.mass = x.mass.replace(/,/g, '');
                });  
            
                // Sort numbers ascending
                people.sort((a, b) => {
                    return a.mass - b.mass;
                });

                // Restore "unknown" values after sorting
                people.forEach(x => {
                    if (x.mass === "0")
                        x.mass = "unknown";
                });
            }
        }
    }
    
    for (let i = 0; i < people.length; i++)
    {
        console.log(people[i].name + " height:" + people[i].height + " mass:" + people[i].mass);
    }

    console.log("\n")
}

router.get('/planets', async (req, res, next) => {
    res.render('planets')
    await processPlanetsRequest(req);
    next();
})

async function processPlanetsRequest(req)
{
    console.log("Fetching & Processing Star Wars Planets From SWAPI (This could take a while...) \n");

    let startingUrl = baseUrl + 'planets/';
    let planetRequests = [];

    let jsonAggregate = [];

    // Get total page count that we'll need to query
    let startingResponse = await axios.get(startingUrl);
    let pageCount = startingResponse.data.count;

    // Aggregate all the planet URLs we'll need to request
    for (let i = 1; i <= pageCount; i++)
    {
        planetRequests[planetRequests.length] = startingUrl + i;
    }

    for (let i = 0; i < planetRequests.length; i++)
    {
        planetResponse = await axios.get(planetRequests[i]);

        // Iterate over planets we recieved in this response
        for (let j = 0; j < planetResponse.data.residents.length; j++)
        {
            // Massage residents property to show standard name rather than API Url
            let residentURL = planetResponse.data.residents[j];
            let residentResponse = await axios.get(residentURL);
            
            planetResponse.data.residents[j] = residentResponse.data.name;
        }

        jsonAggregate[i] = planetResponse.data;
    }   

    console.log(JSON.stringify(jsonAggregate, undefined, 4));
    console.log("\n")
}

module.exports = router